        
       
        
        function generate() 
        {
            var m1 = document.getElementById("m1").value;
            var n1 = 2
            var P = document.getElementById("M1");
            P.innerHTML = "<b> Enter the forces and direction: </b> <br>";
            tablegenerate(document.getElementById("T1"), m1, n1, A1id);               
        }
        function tablegenerate(table, m, n, a) {
            while (table.rows.length > 1)
                table.deleteRow(1);
            for (i = 0; i < m; i++) {

                var r = table.insertRow();
                for (j = 0; j < n; j++) {

                    var c = r.insertCell();
                    t = document.createElement("input");
                    t.type = "number";
                    t.id = a + i + j;
                    c.appendChild(t);
                }
            }
        }
        function solve()
        {
            context.clearRect(0,0,canvas.width,canvas.height)
            context.save();
            context.translate(400,400);
            context.scale(1,-1);

            var total = document.getElementById("m1").value;
            var r=300;
            var Fxres=0,Fyres = 0;
            for (i=0;i<total;i++)
            {
                var angle = (document.getElementById(A1id+i+1).value)*Math.PI/180;
                var force = document.getElementById(A1id+i+0).value;
                var fx=force*Math.cos(angle);
                var fy=force*Math.sin(angle);
                Fxres=fx+Fxres;
                Fyres=fy+Fyres;
                Line(0,0,r*Math.cos(angle),r*Math.sin(angle),"blue",3);
            }
            var F=Math.sqrt(Fxres*Fxres+Fyres*Fyres);
            var qd=1;
            
            
           	var x = Math.sqrt(Fxres*Fxres);
            var y = Math.sqrt(Fyres*Fyres);
            var angle = Math.atan(y/x);
            angle = angle*180/Math.PI;      //Angle in degree (0<=angle<=90)
            if (Fxres<=0&&Fyres>=0)
                angle=180-angle;
            else if(Fxres<=0&&Fyres<=0)             
                angle=270-angle;
            else if (Fxres>=0&&Fyres<=0)             
                angle=360-angle;
            angle=angle*Math.PI/180;
            Line(0,0,r*Math.cos(angle),r*Math.sin(angle),"red",3);
            context.scale(1,-1)
            context.restore();
            var P = document.getElementById("Force");
            P.innerHTML = "<b> The resultant force has a magnitude of </b> " + F;
            var P = document.getElementById("Direction");
            P.innerHTML = "<b> The direction of resultant force (in degree) </b> " + (angle*180/Math.PI);
        }
        function drawtext(text,x0,y0)
        {
            context.font='18pt Calibri';
            context.fillStyle='black';
            context.fillText(text,x0,y0);
        }
        function Line(x0,y0,x,y,color,width)
        {
            context.beginPath();
            context.moveTo(x0,y0);
            context.lineTo(x,y);
            context.strokeStyle=color;
            context.lineWidth=width;
            context.stroke();
        }
        function Rectangle(x0,y0,length,breadth,color,width)
        {
            context.beginPath();
            context.moveTo(x0,y0);
            context.rect(x0,y0,length,breadth)
            context.strokeStyle=color;
            context.lineWidth=width;
            context.stroke();
        }
        function drawArc(x,y,radius,startangle,endangle,color)
        {
            context.beginPath();
            context.strokeStyle=color;
            context.arc(x,y,radius,startangle,endangle,false);
            context.lineWidth=2;
            context.fillStyle=color;
            context.fill();
            context.stroke();
        }
       