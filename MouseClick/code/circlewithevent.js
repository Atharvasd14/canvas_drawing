﻿class myedge {
    constructor(startvertex, endvertex, cost) {
        this.start = startvertex;
        this.end = endvertex;
        this.cost = cost;
    }
    getstart()
    {
        return this.start;
    }
    getend()
    {
        return this.end;
    }
    getcost()
    {
        return this.cost;
    }
}


class mycircle {
    constructor(canvas, x, y, vertex) {
        this.canvas = canvas;
        this.context = this.canvas.getContext("2d");
        this.x = x;
        this.y = y;
        this.r = 25;
        this.vertex = vertex;
        this.x1 = [];
        this.y1 = [];
        this.j = 0;
        this.mycolor = 'red';
    }
    draw() {
        context.beginPath();
        context.arc(this.x, this.y, this.r, 0, 2 * Math.PI, true);
        context.fillStyle = this.mycolor;
        context.fill();
        context.strokeStyle = "black";
        context.lineWidth = 3;
        context.stroke();
    }
    setconnection(x, y) {
        this.x1.push(x);
        this.y1.push(y);
        this.connect = 1;
    }
    drawline() {
        for (var j = 0; j < this.x1.length; j++) {
            this.context.beginPath();
            this.context.moveTo(this.x, this.y);
            this.context.lineTo(this.x1[j], this.y1[j]);
            this.context.strokeStyle = "black";
            this.context.lineWidth = 3;
            this.context.stroke();
        }
    }
}


