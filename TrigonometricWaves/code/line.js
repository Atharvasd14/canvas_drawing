var canvas = document.getElementById("myCanvas");
var context = canvas.getContext("2d");

//for line
document.getElementById('line').addEventListener('click',function()
{
    var x1 = parseInt(document.getElementById("x1").value);
    var y1 = parseInt(document.getElementById("y1").value);
    context.beginPath();
    context.moveTo(0,0);
    context.lineTo(x1,y1);
    context.stroke();
    
});

//for Text
document.getElementById('text').addEventListener('click',function()
{
    var write = document.getElementById("write").value;
    
   
    context.font = "italic 32px sans-serif";
    context.fillText(write,500,250);
    
});

//for rectangle
document.getElementById('rect').addEventListener('click',function()
{
    var x1 = parseInt(document.getElementById("x1").value);
    var y1 = parseInt(document.getElementById("y1").value);
    context.beginPath();
    context.moveTo(0,0);
    context.rect(x1,y1,100,100);
    context.stroke();
    
});

//for triangle
document.getElementById('triangle').addEventListener('click',function(){
    var x1 = parseInt(document.getElementById("x1").value);
    var y1 = parseInt(document.getElementById("y1").value);
    context.beginPath();
    context.moveTo(50,50);
    context.lineTo(50,x1);
    context.lineTo(x1,y1);
    context.lineTo(50,50);
    context.closePath();
    context.fillStyle = '#8ED6FF';
    context.fill();
    context.stroke();
});

//for circle
document.getElementById('circle').addEventListener('click',function(){
    var x1 = parseInt(document.getElementById("x1").value);
    var y1 = parseInt(document.getElementById("y1").value);
    context.beginPath();
    context.arc(x1, y1, 50, 0, 2 * Math.PI, false);

    context.stroke();
})

//clearing the canvas
document.getElementById('clear').addEventListener('click', function() {
        context.clearRect(0, 0, canvas.width, canvas.height);
      }, false);

    